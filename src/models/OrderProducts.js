const { DataTypes } = require('sequelize');


module.exports = (sequelize) => {
  sequelize.define('OrderProducts', {
    order_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Order',
        key: 'id'
      }
    },
    product_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Product',
        key: 'id'
      }
    },
    amount: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  });
}