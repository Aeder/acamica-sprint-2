var router = require('express').Router();

router.use('/users', require('./users'));
router.use('/orders', require('./orders'));
router.use('/products', require('./products'));
router.use('/payment_methods', require('./payment_methods'));

module.exports = router;